import cv2
import numpy as np
import requests


def send_image(source, url="http://127.0.0.1:5000/ref_image"):
    coded = cv2.imencode(".jpg", source)[1]
    file = {"raw_img": ('image.jpg', coded.tostring(), 'image/jpeg', {'Expires': 0})}
    data = {"stab": "232131"}

    response = requests.post(url, files=file, data=data, timeout=5, stream=True)

    return response


cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()

    resp = send_image(frame).json()

    image = resp['img'].encode('ascii')
    image = np.fromstring(image, np.uint8)
    # image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    print(image)

    # cv2.imshow("check", frame)
    # cv2.waitKey(1)
